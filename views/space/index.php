<?php
?>

<div class="col-md-3">
    <img src="img/explorer.jpg" alt="">
</div>

<div class="col-md-6">
    <form method="POST" id="formx" action="javascript:void(null);" class="form-group">
        <h3>Space monkey</h3>
        <div class="form-group">
            <label for="name">Имя*:</label>
            <input id="name" name="name" value="" type="text" class="form-control" autofocus>
            <div id="name-err" class="alert alert-danger hide">
                <strong>Ошибка!</strong> Укажите своё имя!
            </div>
        </div>
        <div class="form-group">
            <label for="surname">Фамилия*:</label>
            <input id="surname" name="surname" value="" type="text" class="form-control">
            <div id="surname-err" class="alert alert-danger hide">
                <strong>Ошибка!</strong> Укажите свою фамилию!
            </div>
        </div>
        <div class="form-group">
            <label for="birthday">Дата рождения:</label>
            <input id="birthday" name="birthday" value="" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label for="company">Компания:</label>
            <input id="company" name="company" value="" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label for="position">Должность:</label>
            <input id="position" name="position" value="" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label for="phone">Телефон*:</label>
            <input id="phone" name="phone" value="" type="text" class="form-control">
            <div id="phone-err" class="alert alert-danger hide">
                <strong>Ошибка!</strong> Укажите телефон для связи!
            </div>
        </div>

        <input id="submit" value="Улететь на марс!" type="submit" class="btn btn-primary">
    </form>

    <div id="success-info" class="alert alert-success hide">
        <strong>Ура!</strong> Ваша заявка принята!
    </div>

    <div id="danger-info" class="alert alert-danger hide">
        <strong>Блин!</strong> Чтото полшло не так, не летим! Проблемы на космодроме!
    </div>
</div>

<div class="col-md-3">
    <img src="img/fermers.jpg" alt="">
</div>