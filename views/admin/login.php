<?php
if(isset($_POST["login"]) && isset($_POST["password"])) {
    if (!App::$identity->auth($_POST["login"], $_POST["password"])) {
        echo "<h2 style=\"color:red;\">Логин и пароль введен не правильно!</h2>";
    } else {
        header('Location: /admin.php');
        exit();
    }
}
?>
    <div style="margin: 0 40%; width: 100px; padding-top: 100px;">
        <form method="post" action="">
            Логин: <input type="text" name="login"
                          value="<?php echo (isset($_POST["login"])) ? $_POST["login"] : null; ?>" />
            <br/>
            Пароль: <input type="password" name="password" value="" /><br/>
            <input type="submit" value="Войти" />
        </form>
    </div>
<?php