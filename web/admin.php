<?php
define('APP_ROOT', dirname(__DIR__));

ini_set('display_errors', true);

require('../App.php');

(new \App([
    'template' => 'admin',
    'route' => 'admin/index',
]))->run();

