function ready () {
    var formx = $('#formx')[0];
    var namei = $('#name')[0];
    var surnamei = $('#surname')[0];
    var birthdayi = $('#birthday')[0];
    var phonei = $('#phone')[0];
    var submit = $('#submit')[0];

    $(birthdayi).inputmask("datetime",{
        mask: "1.2.y",
        placeholder: "dd.mm.yyyy",
        leapday: "29.02.",
        separator: ".",
        alias: "dd.mm.yyyy"
    });

    const require = [namei, surnamei, phonei];

    require.forEach(function (e) {
        e.onchange = function () {
            if(e.value.length > 0) {
                $(this).next().addClass('hide');
                $(this).parent().removeClass('has-error');
                $(submit).removeClass('disabled');
            } else {
                $(this).next().removeClass('hide');
                $(this).parent().addClass('has-error');
            }
        }
    });

    function submitForm() {
        if(validateForm()) {
            var msg = $(formx).serialize();
            $.ajax({
                type: 'POST',
                url: 'index.php?r=space/add',
                data: msg,
                success: function(data) {
                    if(data.success) {
                        $('#success-info').removeClass('hide');
                        $('#danger-info').addClass('hide');

                        $(submit).addClass('disabled');
                        setTimeout(function () {
                            $('#success-info').addClass('hide');
                        }, 5000);
                    } else {
                        $('#danger-info').removeClass('hide');
                    }
                },
                error:  function(xhr, str){
                    console.log('Возникла ошибка: ' + xhr.responseCode);
                    $('#danger-info').removeClass('hide');
                }
            });
        }
    }

    formx.onsubmit = submitForm;

    function validateForm() {
        var success = true;

        require.forEach(function (e) {
            if (!(e.value.length > 0)) {
                success = false;
                $(e).next().removeClass('hide');
                $(e).parent().addClass('has-error');
            }
        });

        if(success && $(submit).hasClass('disabled')) {
            success = false;
        }

        return success;
    }

    phonei.onkeydown = function (e) {
        return notInputTextKey(e.keyCode) || (/^[\+0-9]$/.test(e.key));
    };

    function notInputTextKey(key) {
        return key === 8  ||
               key === 35 ||
               key === 36 ||
               key === 37 ||
               key === 39 ||
               key === 46;
    }
}

document.addEventListener("DOMContentLoaded", ready);