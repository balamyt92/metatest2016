<?php

namespace controllers;

use App;
use Exception;

class BaseController
{
    public function render($page, $option = [])
    {
        $content = $this->renderContent($page, $option);
        $page = $this->renderPhpFile(App::$template, ['content' => $content]);
        echo $page;
        return;
    }

    private function renderPhpFile($file, $option = [])
    {
        ob_start();
        ob_implicit_flush(false);
        extract($option, EXTR_OVERWRITE);
        require($file);
        return ob_get_clean();
    }

    private function renderContent($page, $option = [])
    {
        $file = APP_ROOT . '/views/' . $page . '.php';
        if(is_file($file)) {
            $output = $this->renderPhpFile($file, $option);
            return $output;
        }
        throw new Exception("Нет такого шаблона страницы", 500);
    }

    public function renderJson(array $data)
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        return true;
    }
}