<?php

namespace controllers;

use models\MonkeyModel;

class SpaceController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('space/index');
    }

    public function actionAdd()
    {
        $result = ['success' => false];
        $model = new MonkeyModel();
        $model->load($_POST);
        if($model->validate() && $model->save()) {
            $result['success'] = true;
        }
        return $this->renderJson($result);
    }
}