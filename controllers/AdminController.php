<?php

namespace controllers;


use common\Auth;

class AdminController extends BaseController
{
    public function actionIndex()
    {
        if(\App::$identity->isAuth()) {
            $data = (new \models\MonkeyModel())->findAll();
            return $this->render('admin/index', compact('data'));
        } else {
            return $this->render('admin/login');
        }
    }

    public function actionExit()
    {
        Auth::out();
        header('Location: /admin.php');
        exit();
    }
}