<?php

namespace controllers;

class IndexController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('space/index');
    }

    public function action404()
    {
        return $this->render('index/404');
    }
}