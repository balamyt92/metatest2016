<?php

/**
 * Клас приложения
 */
class App
{
    /**
     * Используемый шаблон страницы
     * шаблоны лежат в папке /views/templates
     *
     * @var string
     */
    static public $template;

    /**
     * Простой роут вида 'controller/action'
     *
     * @var string
     */
    static public $route;

    /**
     * Файл данных
     *
     * @var string
     */
    static public $source;

    /**
     * @var \common\Auth
     */
    static public $identity;

    /**
     * App constructor.
     * @param $config
     */
    public function __construct($config)
    {
        $templatePath = APP_ROOT . "/views/templates";
        App::$template = isset($config['template']) ? $templatePath . '/' . $config['template'] . '.php'
                                                    : "{$templatePath}/main.php";

        App::$route = isset($config['route']) ? $config['route']
                                              : 'index/index';

        App::$source = isset($config['source']) ? $config['source']
                                                : __DIR__ . DIRECTORY_SEPARATOR . 'Monkey.csv';

        if (!file_exists(App::$source)) {
            touch(App::$source);
        }

        $this->autoload();

        App::$identity = new \common\Auth();
    }

    /**
     * Автозагрузка классов
     */
    public function autoload()
    {
        spl_autoload_register(
        function ($class_name) {
            $file = str_replace('\\', DIRECTORY_SEPARATOR, $class_name);
            require APP_ROOT . DIRECTORY_SEPARATOR . $file . '.php';
        });
    }

    public function run()
    {
        try {
            if (isset($_POST['r'])) {
                App::$route = $_POST['r'];
            }
            if (isset($_GET['r'])) {
                App::$route = $_GET['r'];
            }
            return $this->route(App::$route);
        } catch (Exception $e) {
            exit($e->getCode());
        }
    }

    private function route($param)
    {
        $route = explode('/', ucwords($param));
        if(count($route) == 2) {
            $controller = "\\controllers\\{$route[0]}Controller";
            if(class_exists($controller)) {
                $contl = new $controller();
                $action = "action{$route[1]}";
                if(method_exists($contl, $action)) {
                    return $contl->$action();
                }
            }
        }
        return (new \controllers\IndexController())->action404();
    }
}