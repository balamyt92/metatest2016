<?php

namespace common;


class Auth
{
    private $_login;
    private $_password;
    private $secret = 'secret';

    /**
     * Auth constructor.
     */
    public function __construct()
    {
        $this->_login = 'admin';
        $this->_password = md5(md5("admin" . $this->secret));

        session_start();
    }

    /**
     * Проверяет, авторизован пользователь или нет
     *
     * @return boolean
     */
    public function isAuth() {
        if (isset($_SESSION["is_auth"])) {
            return $_SESSION["is_auth"];
        }
        else return false;
    }

    /**
     * Авторизация пользователя
     *
     * @param string $login
     * @param string $password
     * @return bool
     */
    public function auth($login, $password) {
        if ($login == $this->_login && md5(md5($password . $this->secret)) == $this->_password) {
            $_SESSION["is_auth"] = true;
            $_SESSION["login"] = $login;
            return true;
        }
        $_SESSION["is_auth"] = false;
        return false;
    }

    /**
     * @return string
     */
    public function getLogin() {
        if ($this->isAuth()) {
            return $_SESSION["login"];
        }
        return '';
    }


    static public function out() {
        $_SESSION = array();
        session_destroy();
    }
}