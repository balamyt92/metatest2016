<?php

namespace models;


use Exception;

class MonkeyModel
{
    public $name;
    public $surname;
    public $birthday;
    public $company;
    public $position;
    public $phone;

    /**
     * MonkeyModel constructor.
     */
    public function __construct()
    {

    }

    public function load($data)
    {
        $this->name     = isset($data['name'])     ? $data['name'] : '';
        $this->surname  = isset($data['surname'])  ? $data['surname'] : '';
        $this->birthday = isset($data['birthday']) ? $data['birthday'] : '';
        $this->company  = isset($data['company'])  ? $data['company'] : '';
        $this->position = isset($data['position']) ? $data['position'] : '';
        $this->phone    = isset($data['phone'])    ? $data['phone'] : '';
    }

    /**
     * @return bool
     */
    public function validate()
    {
        return $this->name && $this->surname && $this->phone;
    }

    /**
     * Поиск всех
     *
     * @return array
     * @throws Exception
     */
    public function findAll()
    {
        $result = [];
        $maxRow = 1000;
        if (($handle = fopen(\App::$source, "r")) !== FALSE) {
            while (($data = fgetcsv($handle)) !== FALSE && $maxRow) {
                $result[] = $data;
                $maxRow--;
            }
            fclose($handle);
        } else {
            throw new Exception('Не могу прочесть файл данных');
        }
        return $result;
    }

    /**
     * Сохнанение данных
     * @return bool
     */
    public function save()
    {
        if (($handle = fopen(\App::$source, "a")) !== FALSE) {
            fputcsv($handle, [
                $this->name,
                $this->surname,
                $this->birthday,
                $this->company,
                $this->position,
                $this->phone,
            ]);
            fclose($handle);
            return true;
        }
        return false;
    }
}